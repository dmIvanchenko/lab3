import math
import random
import pandas as pd
import numpy as np

from matplotlib import pyplot as plot

# print("Введите название файла:")
# filename = str(input())

tab = pd.read_table("s1.txt", delim_whitespace=True, names=['X', 'Y'])
x1 = tab['X'].to_numpy()
y1 = tab['Y'].to_numpy()

plot.scatter(x1, y1)
plot.savefig('output/no_clasters.png')

print("Введите количество кластеров:")
k = int(input())

counter = len(tab)  # всего строк
center = [0] * k
xCenter = [0.0] * k
yCenter = [0.0] * k

# показывает какая точка к какому кластер у принадлежит
cluster = [0] * counter

# находим первые центроиды случайным образом
numberOfCentroids = 0
for i in center:
    ind = center.index(i)
    randomIndex = int(random.randint(0, counter - 1))
    if randomIndex == 0: numberOfCentroids += 1
    if numberOfCentroids > 1:
        while (randomIndex in center) or (randomIndex == 0):
            randomIndex = int(random.randint(0, counter - 1))
        center[ind] = randomIndex
        xCenter[ind] = tab['X'][randomIndex]
        yCenter[ind] = tab['Y'][randomIndex]
    else:
        center[ind] = randomIndex
        xCenter[ind] = tab['X'][randomIndex]
        yCenter[ind] = tab['Y'][randomIndex]

print(xCenter)
print(yCenter)

# кол-во переходов между кластерами
numberOfSwitches = 1

# кол-во точек в каждом кластере
clusterDotsCounter = [0] * k

while numberOfSwitches > 0:
    numberOfSwitches = 0
    # ищем принадлежность к кластерам
    i = 0
    clusterDotsCounter = [0] * k
    for p in cluster:
        xi = tab['X'][i]  # коорд точки
        yi = tab['Y'][i]

        j = 0
        m = sys.float_info.max
        iMinimal = 0
        for pt in center:
            # расстояние от точки до центров кластера
            distance = math.sqrt(math.pow(xi - xCenter[j], 2) + math.pow(yi - yCenter[j], 2))
            if distance < m:
                m = distance
                iMinimal = j
            j += 1
        # увеличиваем кол-во переходов если новое значение кластера не равно прошлому
        if cluster[i] != iMinimal:
            numberOfSwitches += 1
        cluster[i] = iMinimal
        clusterDotsCounter[iMinimal] += 1
        i += 1
    print(numberOfSwitches)

    # ищем новые центроиды
    i = 0
    for p in center:
        kc = 0.0
        xSum = 0.0
        ySum = 0.0

        j = 0
        for pt in cluster:
            if pt == i:
                kc += 1.0
                xSum += tab['X'][j]
                ySum += tab['Y'][j]
            j += 1

        xCenter[i] = xSum / kc
        yCenter[i] = ySum / kc
        i += 1

print(clusterDotsCounter)
print(sum(clusterDotsCounter))

plot.clf()
i = 0
for pt in center:
    xn = []
    yn = []
    j = 0
    for p in cluster:
        if p == i:
            xn.append(tab['X'][j])
            yn.append(tab['Y'][j])
        j += 1

    plot.scatter(xn, yn)
    i += 1

plot.savefig('output/claster.png')
# plt.show()

# среднее расстояние от точек до центроида по каждому кластеру
averageDotsDistance = []
i = 0
for pt in center:
    s = 0.0
    j = 0
    for p in cluster:
        if p == i:
            distance = math.sqrt(math.pow(tab['X'][j] - xCenter[i], 2) + math.pow(tab['Y'][j] - yCenter[i], 2))
            s += distance
        j += 1
    s /= clusterDotsCounter[i]
    averageDotsDistance.append(s)
    i += 1

print(averageDotsDistance)

figure, ax = plot.subplots()
ox = np.arange(len(center))
ax.bar(ox, height=averageDotsDistance)
ax.set_title("Среднее отклонение от центроидов кластера")
figure.savefig('output/sr.png')